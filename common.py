import re
import pandas as pd

def map_func(x):
    """ Simple occurrence mapper """    
    if not pd.isna(x): #to skip for NaN values
        x=x.strip()
        if re.match(r'^[a-zA-Z0-9]+$', x): #checking to see if the passenger_id is alphanumeric
            return (x, 1)
    

def shuffle(mapper_out):
    """ Organise the mapped values by key """
    data = {}
    mapper_out = list(filter(None, mapper_out)) # to filter out None values created by empty strings/unmatched strings
    for k, v in mapper_out:
        if k not in data:
            data[k] = [v]
        else:
            data[k].append(v)
    return data


def reduce_func_tuple(z):
	""" Simple sum reducer, 1 tuple arg """
	k,v=z
	return (k, sum(v))