from common import map_func, reduce_func_tuple as reduce_func, shuffle
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
from time import perf_counter
import pandas as pd
import sys

start_time = perf_counter()
# using perf_counter to keep track of the time elapsed


if __name__ == '__main__':
    
    # Data Pre-processing
    headers=['passenger_id', 'flight_id', 'from_airport_IATA/FAA_code', 'destination_airport_IATA/FAA_code', \
             'departure_time_GMT', 'total_flight_time_min']
    #input_csv = pd.read_csv('AComp_Passenger_data_no_error.csv', names=headers)
    input_csv = pd.read_csv(sys.argv[1], names=headers)
    print('\n\n\nSize of the input datafile: ', input_csv.shape)
    print('\Information about the dataframe\n')
    print(input_csv.info())
    print('\nFirst 10 records\n', input_csv.head(10))
    #input_csv[input_csv.duplicated()]
    print('\n\nNumber of duplicates, if any: ', input_csv.duplicated().sum())
    input_csv = input_csv.drop_duplicates()
    print('Size of the dataframe after removing the duplicates: ', input_csv.shape)
    
    # Input to the mapper function
    passengers = list(input_csv['passenger_id'])
    map_in = passengers
    
    # Implementing MapReduce using Multithreading
    with ThreadPool(processes=mp.cpu_count()) as pool:
        print('\n\n________________________________________________________________________________\n\n\n \
              Starting MapReduce job with multithreading')
        map_out = pool.map(map_func, map_in, chunksize=int(len(map_in)/mp.cpu_count()))
        #print('output of mapper.py\n', map_out)
        print('\nMap operation successful')
                
        reduce_in = shuffle(map_out)
        #print('\n\n\noutput of shuffle.py\n', reduce_in)
        print('\nShuffle operation successful')
        
        reduce_out = pool.map(reduce_func, reduce_in.items(), chunksize=int(len(reduce_in.keys())/mp.cpu_count()))
        print('\nReduce operation successful\nOutput of reducer.py\n',reduce_out)
        
    # Sort the result of the reducer(list of tuples) in descending order    
    output = sorted(reduce_out,key=lambda x:(-x[1],x[0]))  
    
    # Finding the maximun number of flights taken by any passenger
    max_flights = max(output, key=lambda tup: tup[1])[1]
    print('\n\n\nPassenger(s) having had the highest number of flights are as follows:\n\nID')
    for i in output:
        if i[1] == max_flights:
            print(i[0])
    print(f'\nEach of these passengers took {max_flights} flights')
    
    end_time = perf_counter()
    print(f'\n\nTotal time taken to execute the code: {end_time- start_time: 0.4f} second(s)')


