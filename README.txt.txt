Command: python mt_mapreduce.py <path/filename>
example: python mt_mapreduce.py AComp_Passenger_data_no_error.csv
In this case, the datafile, as well as the Python code, are present in the same directory. 

The first column of the datafile should correspond to the 'Passenger ID'. 